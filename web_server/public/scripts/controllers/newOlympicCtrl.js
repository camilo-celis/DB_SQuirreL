


angular.module('olympicDashboard')
    .controller('NewOlympicCtrl', function($scope, $location, Restangular, countries) {

        console.log("Hello World from new Olympic controller");

        $scope.olympic = {
          year: '',
          season: '',
          city: '',
          country: ''
        };

        $scope.seasons = ['Summer', 'Winter'];
        $scope.country_names = [];
        angular.forEach(countries, function(country) {
          $scope.country_names.push(country.name);
        });

        $scope.submitForm = function() {

          var base = Restangular.all('olympics');

          base.post($scope.olympic).then(function(obj) {
            console.log("Save Success");
            console.log(obj);
            console.log(obj.insertId);
            var path = '/admin/olympics/' + obj.insertId;
            $location.path(path);
          }, function(err) {
            console.log("Error");
            console.log(err);
          });

        };
    });
