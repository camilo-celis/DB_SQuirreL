

'use strict';


angular.module('olympicDashboard')
.controller('HomePageCtrl', function($scope, $rootScope, $q, $timeout, Restangular, ISO3166) {


  $rootScope.$on('$locationChangeSuccess', function() {
    jQuery('.jvectormap-label').each(function(i, elm) {
      jQuery(this).css("display", "none");
    });
  });

  $scope.mapData = {
    markers: [],
    regions: [],
    coordinates: [],
    markerData: []
  };
  //first olympics 1896
  $scope.endRange = 2008;
  $scope.startRange = 2004;
  $scope.selectedInfo = 'athletes';
  $scope.currSport = {};
  $scope.currSport.selected = 'ALL';
  $scope.showPart = false;

  // http://nominatim.openstreetmap.org/search?city=London&country=UK&format=json

  function findCoordinates(city, country) {
    return Restangular.allUrl('coordinates',
                              'http://nominatim.openstreetmap.org/search?accept-language=en&city=' + city +
                              '&countrycodes=' + country + '&format=json').getList();
  }

  function refreshMarkers(start, end){
    var temp = angular.copy($scope.mapData);

    var promises = [];
      Restangular.all('olympics').customGETLIST('summary', {
        startYear: start,
        endYear: end
      }).then(function(rows) {
        var allCoords = [];
        var coordData = [];
        for (var i = 0; i < rows.length; i++) {
          var curr = rows[i];
          var currCity = curr.city;
          var currCountry = ISO3166.getCountryCode(curr.country);
          if (currCountry) {
            var res = findCoordinates(currCity, currCountry);
            promises.push(res);

            res.then(function(res) {
              if (res && res.length > 0) {
                var k = res[0];
                allCoords.push([k.lat, k.lon]);
                coordData.push({data: k.display_name});
              }
            }, function(err) {
              console.log(err);
            });
          }

          //temp.coordinates = allCoords;
          $q.all(promises).then(function(res) {
            var array = [];
            for (var j = 0; j < allCoords.length; j++) {
              array.push(1);
            }

            temp.coordinates = allCoords;
            temp.markers = [];
            temp.markers.push(array);
            temp.markerData = [];
            temp.markerData.push(coordData);

            $scope.mapData = angular.copy(temp);

          }, function(err) {
            console.log("Error occurred while fetching coordinates (promise)");
            console.log(err);
          });

        }
      }, function(err) {
        console.log("Error occurred while fetching city coordinates");
        console.log(err);
      });
  }

  function refreshMap(start, end, info, sport) {
    var selected = Restangular.all('olympics').all(info);
    selected.customGETLIST('summary', {
      startYear: start,
      endYear: end,
      sportName: sport
    }).then(function(rows) {

      var res = {};
      angular.forEach(rows, function(val) {
        var m = ISO3166.getCountryCode(val.country);
        if (m == undefined) {
          console.log("Undefined: " + val.country);
        };
        res[ISO3166.getCountryCode(val.country)] = val.selected_count;
      });

      var temp = angular.copy($scope.mapData);
      temp.regions = [];
      temp.regions.push(res);

      $scope.mapData = angular.copy(temp);
      refreshMarkers(start, end);
    }, function(err) {
      console.log("An Error Occurred");
      console.log(err);
    });

  }

  function updateParticipatingCountries() {
    Restangular.all('olympics').all('countries').customGETLIST('summary', {
        startYear: $scope.startRange,
        endYear: $scope.endRange,
        sportName: $scope.currSport.selected
    }).then(function(res) {

      var result = {};
      for (var i = 0; i < res.length; i++) {
        var code = ISO3166.getCountryCode(res[i].country_name);
        if (code) {
          result[code] = 1;
        }
      }

      var temp = angular.copy($scope.mapData);
      temp.regions = [];
      temp.regions.push(result);
      $scope.mapData = angular.copy(temp);
      refreshMarkers($scope.startRange, $scope.endRange);
    }, function(err) {
      console.log("An Error occurred while updating the Participating countries");
      console.log(err);
    })
  }

  // First-time renderin
  refreshMap($scope.startRange, $scope.endRange, $scope.selectedInfo, $scope.currSport.selected);
  //updateParticipatingCountries();

  var timeoutPromise;
  var delayInMs = 1000;

  $scope.$watch('endRange', function(newVal) {
    $timeout.cancel(timeoutPromise);  //does nothing, if timeout alrdy done
    timeoutPromise = $timeout(function(){   //Set timeout
      if ($scope.showPart) {
        updateParticipatingCountries();
      } else {
        refreshMap($scope.startRange, newVal, $scope.selectedInfo, $scope.currSport.selected);
      }
    },delayInMs);
  });

  $scope.$watch('startRange', function(newVal) {
    $timeout.cancel(timeoutPromise);  //does nothing, if timeout alrdy done
    timeoutPromise = $timeout(function(){   //Set timeout
      if ($scope.showPart) {
        updateParticipatingCountries();
      } else {
        refreshMap(newVal, $scope.endRange, $scope.selectedInfo, $scope.currSport.selected);
      }
    }, delayInMs);

  });

  $scope.$watch('selectedInfo', function(newVal) {
    if (newVal == "athletes") {
      refreshMap($scope.startRange, $scope.endRange, "athletes", $scope.currSport.selected);
      $scope.showPart = false;
    } else if (newVal == "medals") {
      refreshMap($scope.startRange, $scope.endRange, "medals", $scope.currSport.selected);
      $scope.showPart = false;
    } else {
      updateParticipatingCountries();
      $scope.showPart = true;
    }
  });

  $scope.$watch('currSport.selected', function(newVal) {
    if ($scope.selectedInfo == 'part') {
      updateParticipatingCountries();
    } else {
      refreshMap($scope.startRange, $scope.endRange, $scope.selectedInfo, newVal);
    }
    $scope.modalShown = false;
  });

  $scope.modalShown = false;
  $scope.toggleModal = function() {
    $scope.modalShown = !$scope.modalShown;
  };

  $scope.sports = [];
  Restangular.all('sports').getList().then(function(res) {
    $scope.sports = res;
  }, function(err) {
    console.log("An Error occurred while fetching sports");
    console.log(err);
  });

  // Slider
  $("#year_slider").ionRangeSlider({
      min: 1890,
      max: 2020,
      from: 2004,
      to: 2008,
      type: 'double',
      maxPostfix: "+",
      prettify: false,
      hasGrid: true,
      onChange: function(obj) {
        $scope.$apply(function() {
          $scope.startRange = obj.fromNumber;
          $scope.endRange = obj.toNumber;
        });
      }
  });
});
