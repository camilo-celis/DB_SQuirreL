
angular.module('olympicDashboard')
    .controller('LoginCtrl', function($rootScope, $scope, Restangular, $location, SessionService) {

        $scope.hasError = false;
        $scope.errorMessage = "";

        $scope.username = "";
        $scope.password = "";
        $scope.submitForm = function() {
          Restangular.all('sessions').customPOST({
            username: $scope.username,
            password: $scope.password
          }, 'login').then(function(res) {
            console.log(res);
            SessionService.setToken(res.token);
            $rootScope.isLoggedIn = true;
            $rootScope.username = $scope.username;
            console.log($rootScope.username)

            $scope.hasError = false;
            $scope.errorMessage = "";

            $rootScope.logOut = function(){
                SessionService.setToken('');
                $location.path('/');
                $rootScope.isLoggedIn = false;
            }

            $location.path('/admin/olympics');


          }, function(err) {
            console.log(err);
            console.log("An error occurred while logging in");
            $scope.errorMessage = "An Error occurred while logging in";
            $scope.hasError = true;
          });
        }

    });
