

angular.module('olympicDashboard')
    .controller('EditOlympicCtrl', function($scope, $location, Restangular, currOlympic, countries) {

        console.log("Hello World from new Olympic controller");

        $scope.olympic = currOlympic;
        $scope.seasons = ['Summer', 'Winter'];
        //$scope.seasons = [{name: 'Summer'}, {name: 'Winter'}];
        $scope.country_names = [];
        angular.forEach(countries, function(country) {
          $scope.country_names.push(country.name);
        });

        $scope.submitForm = function() {
          $scope.olympic.country = 'South Korea';
          $scope.olympic.put().then(function(res) {
            console.log("Edit Successed");
            console.log(res);
            var path = '/admin/olympics/' + $scope.olympic.id;
            $location.path(path);
          }, function(err) {
            console.log("An Error Occurred");
            console.log(err);
          });
        };

    });
