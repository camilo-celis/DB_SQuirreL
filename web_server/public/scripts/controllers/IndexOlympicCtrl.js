

angular.module('olympicDashboard')
    .controller('IndexOlympicCtrl', function($scope, Restangular, $location, olympics) {

        console.log("Hello World from new Olympic Index controller");
        $scope.olympics = olympics;

        $scope.hideAlertBox = function() {
        	jQuery("#myAlertBox").hide();
        }
    });
