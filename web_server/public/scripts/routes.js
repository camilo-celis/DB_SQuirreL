"use strict"


angular.module('olympicDashboard')
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                redirectTo: '/home'
            })
            .when('/login', {
              templateUrl: '../views/login.html',
              controller: 'LoginCtrl'
            })
            .when('/home', {
                templateUrl: '../views/home.html',
                controller: 'HomePageCtrl'
            })
            .when('/about' ,{
                templateUrl: '../views/about.html',
                controller: 'aboutCtrl'
            })
            .when('/olympics', {
                templateUrl: '../views/olympics/userindex.html',
                controller: 'IndexOlympicCtrl',
                resolve: {
                  olympics: function(Restangular) {
                    return Restangular.all('olympics').getList();
                  }
                }
            })
            .when('/admin/olympics', {
                templateUrl: '../views/olympics/index.html',
                controller: 'IndexOlympicCtrl',
                resolve: {
                  olympics: function(Restangular) {
                    return Restangular.all('olympics').getList();
                  },
                  token: function(Restangular) {
                    return Restangular.all('sessions').customGET('checkToken').get();
                  }
                }
            })
            .when('/admin/olympics/new', {
                templateUrl: '../views/olympics/new.html',
                controller: 'NewOlympicCtrl',
                resolve: {
                  countries: function(Restangular) {
                    return Restangular.all('countries').getList();
                  },
                  token: function(Restangular) {
                    return Restangular.all('sessions').customGET('checkToken').get();
                  }
                }
            })
            .when('/admin/olympics/:olympicId/edit', {
                templateUrl: '../views/olympics/edit.html',
                controller: 'EditOlympicCtrl',
                resolve: {
                  currOlympic: function(Restangular, $route) {
                    return Restangular.one('olympics', $route.current.params.olympicId).get();
                  },
                  countries: function(Restangular) {
                    return Restangular.all('countries').getList();
                  },
                  token: function(Restangular) {
                    return Restangular.all('sessions').customGET('checkToken').get();
                  }
                }
            })
            .when('/admin/olympics/:olympicId', {
                templateUrl: '../views/olympics/show.html',
                controller: 'ShowOlympicCtrl',
                resolve: {
                  currOlympic: function(Restangular, $route) {
                    return Restangular.one('olympics', $route.current.params.olympicId).get();
                  },
                  token: function(Restangular) {
                    return Restangular.all('sessions').customGET('checkToken').get();
                  }
                }
            })
            .when('/olympics/:olympicId', {
                templateUrl: '../views/olympics/usershow.html',
                controller: 'ShowOlympicCtrl',
                resolve: {
                  currOlympic: function(Restangular, $route) {
                    return Restangular.one('olympics', $route.current.params.olympicId).get();
                  }
                }
            })
            .otherwise({
                redirectTo: '/home'
            });
    });
