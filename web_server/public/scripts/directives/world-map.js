
'use strict';

angular.module('WorldMapDirective', [])
.directive("worldmap", function() {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      data: '='
    },
    templateUrl: '../../views/world-map/world-map.html',
    link: function(scope, element, attrs) {

      var map = {};
      function render(currData) {
        $('.jvectormap-label').each(function(i, elm) {
          $(this).css("display", "none");
        });

        var elm = angular.element('<div style="height:' + (attrs.mapHeight || '600px') + '" id="' + attrs.name + '"></div>');
        element.children(0).remove();
        element.append(elm);

        var currRegions = [];
        var currMarkers = [];

        for(var i = 0; i < currData.regions.length; i++) {
          currRegions.push({
            values: currData.regions[i],
            //scale: ['#FFFFFF', '#DC143C'],
            scale: ['#C8EEFF', '#0071A4'],
            min: jvm.min(currData.regions[i]),
            max: jvm.max(currData.regions[i]),
            normalizeFunction: 'polynomial'
          });
        }

        for(var i = 0; i < currData.markers.length; i++) {
          currMarkers.push({
            values: currData.markers[i],
            attribute: 'fill',
            scale: ['#FFE4C4', '#FFEBCD'],
            min: jvm.min(currData.markers[i]),
            max: jvm.max(currData.markers[i])
          });

          currMarkers.push({
            attribute: 'r',
            scale: [1, 3],
            values: currData.markers[i]
          });
        }


        jQuery('#' + attrs.name).vectorMap({
          map: 'world_mill_en',
          markers: currData.coordinates,
          series: {
            markers: currMarkers,
            regions: currRegions
          },
          onRegionLabelShow: function(e, el, code) {
            var firstRegion = scope.data.regions[0];
            el.html(
              ''+el.html() + '' + '<br>Count - ' + (firstRegion[code] ? firstRegion[code] : 0) + ' '
            );
          },
          onMarkerLabelShow: function(event, label, index){

            var currLayer = scope.data.markerData[0];
            var curr = currLayer[index];
            label.html(
              ''+curr.data+''
            );
          }
        });
      }


      //First time create the map
      render(scope.data);

      // var obj = jQuery('#' + attrs.name).vectorMap('get', 'mapObject');
      function updateData(newData) {
        console.log(map);
        //map.series.regions[0].clear();
        //map.series.regions[0].setNormalizeFunction('polynomial');
        var map = jQuery('#' + attrs.name).vectorMap('get', 'mapObject');
        map.series.regions[0].setValues(newData.regions[0]);

        map.series.markers[0].setValues(scope.data.markers[0]);
        map.series.markers[1].setValues(scope.data.markers[0]);
        //console.log(scope.data.regions[0]);
      }


      scope.$watch("data", function(newData) {
        render(newData);
        //updateData(newData);
      });
    }
  }
});
