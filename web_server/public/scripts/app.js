"use strict"

angular.module('olympicDashboard',
              ['ngResource',
              'restangular',
              'ngRoute',
              'caco.ClientPaginate',
              'vr.directives.slider',
              'WorldMapDirective',
              'WorldMapSelectedDirective',
              'iso-3166-country-codes',
              'DB.modal',
              'so.groupBy']);


// Send the token (if available) with every $http request
// If any of the response from $http or $resource results in HTTP 401 (Unauthorized) redirect to login page
angular.module('olympicDashboard')
.factory('authInterceptor', function($rootScope, $q, $window, $location) {
  return {
    request: function(config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers.token = $window.sessionStorage.token;

        $rootScope.isLoggedIn = true;
      }
      return config;
    },
    response: function(response) {
      // Not Authorized - Redirect back to login page
      if (response.status === 401) {
        $rootScope.isLoggedIn = false;

        delete $window.sessionStorage.token;
        $location.path('/login');

      }
      return (response || $q.when(response));
    },
    responseError: function(rejection) {

      if (rejection.status === 401) {
        $rootScope.isLoggedIn = false;

        delete $window.sessionStorage.token;
        $location.path('/login');
      } else {
        console.log('Response Error ');
        //$window.history.back();
      }

      return $q.reject(rejection);
    }
  };
});

angular.module('olympicDashboard')
.run(function($rootScope, $window, SessionService, $location){

  if($window.sessionStorage.token)
    $rootScope.isLoggedIn = true;
  else
    $rootScope.isLoggedIn = false;

  $rootScope.logOut = function(){
    SessionService.setToken('');
    $location.path('/');
    $rootScope.isLoggedIn = false;
   }
});

angular.module('olympicDashboard')
.config(function($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
});
