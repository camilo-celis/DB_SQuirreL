'use strict';


// Olympics Service
angular.module('olympicDashboard')
.factory('Olympic', function($resource) {
  return $resource('/olympics/:olympicId',
  { olympicId: "@id" },
  {update: {method: 'PUT'}
});
});
