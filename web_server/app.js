
/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var exphbs = require('express3-handlebars');

// Express Server
var app = express();

// all environments
app.engine('handlebars', exphbs({defaultLayout: 'layout'}));
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.bodyParser());

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Load our Database Wrapper class
var db = require('./modules/database.js');

// routes
// we have to pass the db class to the routes, so that we can share the db pool
require('./modules/routes.js')(app, db);

// Start the webser

var server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


exports = module.exports = app;
