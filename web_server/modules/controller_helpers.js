
'use strict';

var Q = require('q');

module.exports.sendDBResponse = function(db, query, req, res, isNotArray) {
  console.log(query);
  var promise = db.doQuery(query);
  promise.then(function(rows) {
    if (isNotArray) {
      if (rows == [])
        res.send({});
      else
        res.send(rows[0]);
    } else {
      res.send(rows);
    }
  }, function(err) {
    console.log(query);
    console.log("An Error occurred");
    console.log(err);
    res.status(500);
    res.send(err);
  });
};

module.exports.sendToCallback = function(db, query, success, res, isNotArray) {
  console.log(query);
  var promise = db.doQuery(query);
  promise.then(function(rows) {
    if (isNotArray) {
      if (rows == [])
        success({});
      else
        success(rows[0]);
    } else {
      success(rows);
    }
  }, function(err) {
    console.log(query);
    console.log("An Error occurred");
    console.log(err);
    res.status(500);
    res.send(err);
  });
};

module.exports.sendDBResponseTimed = function(db, query, req, res, limit, isNotArray) {
  console.log(query);
  var promise = db.doTimedQuery(query, limit);
  promise.then(function(rows) {
    if (isNotArray) {
      if (rows == [])
        res.send({});
      else
        res.send(rows[0]);
    } else {
      res.send(rows);
    }
  }, function(err) {
    console.log(query);
    console.log("An Error occurred");
    console.log(err);
    res.status(500);
    res.send(err);
  });
};

module.exports.sendToCallbackTimed = function(db, query, success, res, limit, isNotArray) {
  console.log(query);
  var promise = db.doTimedQuery(query, limit);
  promise.then(function(rows) {
    if (isNotArray) {
      if (rows == [])
        success({});
      else
        success(rows[0]);
    } else {
      success(rows);
    }
  }, function(err) {
    console.log(query);
    console.log("An Error occurred");
    console.log(err);
    res.status(500);
    res.send(err);
  });
};
