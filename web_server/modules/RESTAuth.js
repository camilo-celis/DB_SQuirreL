'use strict';

var jwt = require('./jwt.js');
var helper = require('./controller_helpers.js');

// Authenticates the token,
// onSuccess: attaches 'user' and 'token' to 'req'
module.exports = function(db) {
  return {
    authenticate_token: function(req, res, next) {
      var token = req.headers.token;
      if (token) {
        // TODO: Try catch Token in Token model
        token = jwt.decode(token);

        console.log(token);

        if (token && token.username && token.exp) {
          if (token.exp < Date.now()) {
            res.json(401, {
              error: {
                message: 'Unathorized Access - Expired Token'
              }
            });
          }
          var query = 'SELECT * FROM users WHERE email = ' + '\'' + token.username + '\'';
          helper.sendToCallbackTimed(db, query, function(rows) {
            req.isLoggedIn = true;
            next();
          }, res, 1000);

        } else {
          res.json(401, {
            error: {
              message: 'Unathorized Access - inValid Token'
            }
          });
        }

      } else {
        res.json(401, {
          error: {
            message: 'Unathorized Access - No token in the header or inV'
          }
        });
      }

    }
  }
};
