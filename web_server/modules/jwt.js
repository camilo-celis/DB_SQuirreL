'use strict';

var jwt = require('jwt-simple'),
          tokenSecret = 'My Token Secret';

module.exports = {
  encode: function(data) {
    var result = null;
    try {
      result = jwt.encode(data, tokenSecret);
    } catch (e) {
      result = null;
    }
    return result;
  },
  decode: function(payload) {
    var result = null;
    try {
      result = jwt.decode(payload, tokenSecret);
    } catch(e) {
      result = null;
    }
    return result;
  }
}
