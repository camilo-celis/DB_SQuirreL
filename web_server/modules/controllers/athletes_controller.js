
'use strict';

// Atheletes Controller

// Require the Promise Library
var helper = require('../controller_helpers');


module.exports = function(db) {
  return {
    index: function(req, res) {
      // HERE the QUERY
      // Since Athelete is a nested resource
      // either olympicId or countryId must be defined
      var olympic_id = req.currOlympic ? req.currOlympic.id : undefined ;
      var country_name = req.currCountry ? req.currCountry.name : undefined;
      if (olympic_id) {
        // FIX the query (Have to join with some table)
        var query = 'SELECT * FROM athlete join olympic_athletes on (athlete.id = olympic_athletes.athlete_id)' +
                    'WHERE olympic_id = ' + olympic_id;
        helper.sendDBResponseTimed(db, query, req, res, 1000);
      } else if (country_name) {
        var query = 'SELECT * FROM athlete ' +
                    'WHERE country = ' + country_name;
        helper.sendDBResponseTimed(db, query, req, res, 1000);
      } else {
        res.send("olympicId or countryName not found");
      }
    },
    mapID: function (req, res, next, id) {
      if (id) {
        var query = 'SELECT * FROM athlete WHERE id = ' + id;
        helper.sendToCallbackTimed(db, query, function(rows) {
          req.currAthlete = rows[0];
          next();
        }, res, 1000);
      } else {
        res.send("athleteId is not defined");
      }
    },
    summary: function(req, res) {
      var startYear = req.param('startYear');
      var endYear = req.param('endYear');
      var sport_name = req.param('sportName');

      if (startYear && endYear && sport_name) {
        var query = 'SELECT athlete.country, COUNT(*) as selected_count ' +
                    'FROM athlete JOIN olympic_athletes on (athlete.id = olympic_athletes.athlete_id) ' + ' ' +
                                            ' JOIN olympic on (olympic_athletes.olympic_id = olympic.id)' + ' ' +
                    'WHERE olympic.year >= ' + startYear + ' and olympic.year <= ' + endYear + ' ' +
                    ' ' + (sport_name != "ALL" ? ("AND athlete.sport = " + '\'' + sport_name + '\'') : " " ) +
                    ' GROUP BY athlete.country';

        helper.sendDBResponse(db, query, req, res);
      } else {
        res.status(500);
        res.send("startYear or endYear and sport_name not found");
      }
    },
    show: function(req, res) {
      var athlete_id = req.currAthlete ? req.currAthlete.id : undefined ;
      var query = 'SELECT * FROM athlete ' +
                  'WHERE athlete_id = ' + athlete_id ;
      helper.sendDBResponseTimed(db, query, req, res, 1000);
    },
    update: function(req, res) {
      var id = req.body.id;
      var first_name = req.body.first_name;
      var middle_name = req.body.middle_name ? req.body.middle_name : "";
      var last_name = req.body.last_name;
      var dob = req.body.dob ? req.body.dob : "";
      var photo = req.body.photo ? req.body.photo : "";
      var country = req.body.country;
      var sport = req.body.sport;

      var query = 'UPDATE athlete ' +
                  'SET first_name = '+ '\'' + first_name + '\'' + ' ' +
                  'SET middle_name = '+ '\'' + middle_name + '\'' + ' ' +
                  'SET last_name = '+ '\'' + last_name + '\'' + ' ' +
                  'SET year = ' + year + ' ' +
                  'SET dob = ' + dob + ' ' +
                  'SET photo = '+ '\'' + photo + '\'' + ' ' +
                  'SET country = '+ '\'' + country + '\'' + ' ' +
                  'SET sport = '+ '\'' + sport + '\'' + ' ' +
                  'WHERE id = ' + id;

      helper.sendDBResponseTimed(db, query, req, res, 1000);
    }
  };
}
