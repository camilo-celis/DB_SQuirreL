
'use strict';

// Sessions Controller

var helper = require('../controller_helpers');
var jwt = require('../jwt.js');

module.exports = function(db) {
  return {
    login: function(req, res) {
      // TODO: if username and password is correct
      var username = req.body.username;
      var password = req.body.password;

      var query = 'SELECT * FROM users WHERE email = ' + '\'' + username + '\'' +
                    ' AND password = ' + '\'' + password + '\'' + ' ';

      helper.sendToCallback(db, query, function(rows) {
        // Expires after a few time
        if (rows.length > 0) {
          var token = jwt.encode({username: username, exp: Date.now() + 300000});
          console.log(token);
          res.send({token: token});
        } else {
          res.json(401, {
            error: {
              message: 'Unathorized Access - User Not Foun'
            }
          });
        }
      }, res);

    },
    checkToken: function(req, res) {
      // This one is okay, don't modify it
      res.send('Okay');
    }
  };
}
