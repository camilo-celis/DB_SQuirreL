
'use strict';

// Sports controller


var helper = require('../controller_helpers');

module.exports = function(db) {
  return {
    index: function(req, res) {
      // HERE the QUERY
      // Since Sport is a nested resource
      // either olympicId or countryId must be defined

      var olympic_id = req.currOlympic ? req.currOlympic.id : undefined ;
      var country_name = req.currCountry ? req.currCountry.name : undefined;
      if (olympic_id) {
        // FIX the query (Have to join with some table)
        var query = 'SELECT * FROM olympic_sport natural join sport ' +
                    'WHERE olympic_id = ' + olympic_id;
        helper.sendDBResponseTimed(db, query, req, res, 1000);
      } else if (country_name) {
        var query = 'SELECT UNIQUE(sport) FROM athlete ' +
                    'WHERE country = ' + country_name;
        helper.sendDBResponseTimed(db, query, req, res, 1000);
      } else {
        var query = 'SELECT * FROM sport';
        helper.sendDBResponse(db, query, req, res);
      }
    },
    mapID: function (req, res, next, name) {
      if (name == null)
        res.send("sportId can't be null");
      else {
        var query = 'SELECT * FROM sport WHERE name = ' + name;
        // If olympicId doesn't exist, send back the res error, o.w. continue
        helper.sendToCallbackTimed(db, query, function(rows) {
          req.currSport = rows[0];
          next();
        }, res, 1000);
      }
    },
    show: function(req, res) {
      var sport_name = currSport.name;
      var query = 'SELECT * FROM sport WHERE name = ' + name;
      helper.sendDBResponseTimed(db, query, req, res, 1000);
    },
    summary: function(req, res) {
            var startYear = req.param('startYear');
      var endYear = req.param('endYear');
      if (startYear && endYear) {
        var query = 'SELECT athlete.country, COUNT(DISTINCT sport) as selected_count ' +
                    'FROM athlete JOIN olympic_athletes on (athlete.id = olympic_athletes.athlete_id) ' + ' ' +
                              ' JOIN olympic on (olympic_athletes.olympic_id = olympic.id)' + ' ' +
                    'WHERE olympic.year >= ' + startYear + ' and olympic.year <= ' + endYear + ' ' +
                    'GROUP BY athlete.country';

        helper.sendDBResponse(db, query, req, res);
      } else {
        res.status(500);
        res.send("startYear or endYear not found");
      }
    }
  };
}
