"use strict"

// Country Controller

var helper = require('../controller_helpers');

module.exports = function(db) {
  return {
    index: function(req, res) {
      // TODO: Check the query
      // TODO: Create a page number option % 10
      var query = 'SELECT * FROM country';
      helper.sendDBResponseTimed(db, query, req, res, 1000);
    },
    mapID: function (req, res, next, name) {
      if (!name) {
        res.status(500);
        res.send("Country name can't be null");
      }
      else {
        var query = 'SELECT * FROM country '+
                    'WHERE name = ' + '\'' + name + '\'';

        helper.sendToCallbackTimed(db, query, function(row) {
          req.currCountry = row;
          next();
        }, res, 1000, true);
      }
    },
    show: function(req, res){
      var name = req.currCountry.name;

      var query = 'SELECT * FROM country '+
                  'WHERE name=' + '\'' + name + '\'';

      helper.sendDBResponseTimed(db, query, req, res, 3000, true);
    },
    create: function(req, res){
      var name = req.body.name;
      var capital = req.body.capital;

      console.log(req.body);

      var query = 'INSERT INTO country(name, capital)' +
                  'VALUES (' + '\'' + name + '\'' + ', ' + '\'' + capital + '\'' + ')';

      helper.sendDBResponseTimed(db, query, req, res, 3000);
    },
    update: function(req, res){
      var name = req.currCountry.name;
      var new_capital = req.body.capital;

      var query = 'UPDATE country ' +
                  'SET capital='+ '\'' + new_capital + '\'' +
                  'WHERE name=' + '\'' + name + '\'';

      helper.sendDBResponseTimed(db, query, req, res, 1000);
    },
    delete: function(req, res){
      var name = req.currCountry.name;

      var query = 'DELETE '+
                  'FROM country' +
                  'WHERE name=' + '\'' + name + '\'';

      helper.sendDBResponseTimed(db, query, req, res, 1000);
    }
  };
}
