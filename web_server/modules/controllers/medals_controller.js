
'use strict';

// Medals controller


// Require the Promise Library

var helper = require('../controller_helpers');

module.exports = function(db) {
  return {
    index: function(req, res) {
      // HERE the QUERY
      // Since Medal is a nested resource
      // either olympicId or countryId must be defined
      var olympic_id = req.currOlympic ? req.currOlympic.id : undefined ;
      var country_name = req.currCountry ? req.currCountry.name : undefined;
      if (olympic_id) {
        // FIX the query (Have to join with some table)
        var query = 'SELECT * FROM medal join athlete on (athlete_id = athlete.id)' +
                    ' WHERE olympic_id = ' + olympic_id;
        helper.sendDBResponseTimed(db, query, req, res, 1000);
      } else if (country_name) {
        var query = 'SELECT * FROM medal JOIN athlete on athlete_id = athlete.id' +
                    'WHERE country = ' + country_name;
        helper.sendDBResponseTimed(db, query, req, res, 1000);
      } else {
        res.send("olympicId or countryName not found");
      }

    },
    show: function(req, res) {
      // AtheleteID should defined when this function is called
      var olympic_id = req.currOlympic ? req.currOlympic.id : undefined ;
      var athlete_id = req.currAthlete ? req.currAthlete.id : undefined ;

      var query = 'SELECT * FROM medal ' +
                  'WHERE olympic_id = ' + olympic_id + ' and athlete_id = ' + athlete_id;

      helper.sendDBResponseTimed(db, query, req, res, 1000);
    },
    summary: function(req, res) {
      var startYear = req.param('startYear');
      var endYear = req.param('endYear');
      var sport_name = req.param('sportName');

      if(startYear && endYear && sport_name) {
        var query = 'SELECT athlete.country, COUNT(*) as selected_count ' +
                    'FROM medal JOIN athlete on (athlete_id = athlete.id) ' +
                              ' JOIN olympic_athletes on (athlete.id = olympic_athletes.athlete_id) ' + ' ' +
                              ' JOIN olympic on (olympic_athletes.olympic_id = olympic.id)' + ' ' +
                    'WHERE olympic.year >= ' + startYear + ' and olympic.year <= ' + endYear + ' ' +
                    ' ' + (sport_name != "ALL" ? ("AND medal.sport = " + '\'' + sport_name + '\'') : " " ) +
                    ' GROUP BY athlete.country';

        helper.sendDBResponse(db, query, req, res);
      } else {
        res.status(500);
        res.send("startYear, endYear or sport_name not found");
      }
    }
  };
}
