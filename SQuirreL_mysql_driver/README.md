SQuirreL MySQL Driver
==========================

A driver that takes SQL Strings as input and executes it in MySQL. The return value will be a Promise, that will be fulfilled as the Database responses.


Main Features
---------------------

Should be compatible with NodeJS

The driver is mainly divided into two parts. 

	- It takes SQL statements as input and returns a Promise, which will be fulfilled with the response
	- It Serializes the returned response into JS Objects for Node
	- (Optional) Provides a simple API, that will generate SQL Command Strings based on the SQL command type
		- MySQLConnection myConn = new MySQLConnection(....);
		- myConn.select("id,name,age").from("atheletes_table").where("id = 2342").get();

