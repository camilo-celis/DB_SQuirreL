-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table `users`
-- 
-- ---

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users`(
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(30) NULL,
	`email` VARCHAR(50) NOT NULL,
	`password` VARCHAR(180) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=24762 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `users` WRITE;

INSERT INTO `users` (`username`,`email`,`password`) VALUES
('cacyss0807','camilo.celis0807@gmail.com','cacyss0807'),
('zeronone','majafer10@gmail.com','zeronone'),
('TaeHunKim','rapaellk29@gmail.com','TaeHunKim'),
('jake','jakewfortescue@gmail.com','jake'),
('db','db','db');

UNLOCK TABLES;