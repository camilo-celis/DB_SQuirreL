Contribution Rules
==================

Please keep the following rules while contributing.

- Modular Approach (please do your project in a seperate folder, so that they do not cause any merge conflicts)
- Tentative features must be checked out into a new branch, and after full testing, it should be merged with 'stable' branch
- DONT merge to 'stable' branch until fully tested, or code that have bugs
- You can merge other branches to the master branch anytime, 

- Please keep the commits short
- Please write a brief outline of changes in your commit


master branch will be the most current working development snapshot

stable branch will have be only fully tested and running code



