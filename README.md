#DB SQuirreL
*Seoul National University* / *Introduction to databases* / *2014 Spring Semester* / *Final Project*

*Arif Jafer* / *Camilo A. Celis Guzman* / *Jake Forescue* / *Kim Tae-Hun*

> We recomend to view this file using a [`markdown viewer`](http://markable.in/) or use the `HTML` version. Otherwise, please refer to the project's repository on [`Github`](https://github.com/cacyss0807/DB_SQuirreL.git)

## 1. Install the necessary packages

I. Install [`Node.js`](http://www.nodejs.org/download/)

> If you are working on Ubuntu, **DO NOT** install it via `apt-get`. That installs a lower version. Installing nodejs via its webpage is recommended. 


II. Install [`MySQL`](http://dev.mysql.com/downloads/mysql/)

> If you are working on Ubuntu, enter: `sudo apt-get install mysql-client mysql-server` in the terminal.


III. From the `web_server` directory (inside our's project directory) run:

  - Install node express `npm install -g express`
  - Installed bower `npm install bower -g`
  - Installed angular.js `bower install angular`
  - Install missing packages `npm install`
  - Install missing packages `bower install`
  
> - To use bower, git must be installed. 
- If `bower install` is not working, use `node_modules/bower/bin/bower install`(Linux or OSX) or `node_modules\bower\bin\bower install` (Windows).


## 2. Setting MySQL

I. Setting a MySQL's new user

  `$ mysql --user=root mysql`
  
> If you have assigned a password to the root account, you will also need to supply a `--password` or `-p` option.
  
> If you use MySQL on Windows Environment, run `MySQL 5.6 Command Line Client` instead of this step.
  
II. Create a new user

  `mysql> CREATE USER 'squirrel'@'localhost' IDENTIFIED BY 'squirrel';`

  `mysql> GRANT ALL PRIVILEGES ON *.* TO 'squirrel'@'localhost' WITH GRANT OPTION;`

> If you want to set different username or password, you must edit 'user' and 'password' items in the following paths to your username and password.

- `webserver/config/db/development.js` 
- `webserver/config/db/production.js`
- `webserver/modules/config/db/development.js`
- `webserver/modules/config/db/production.js`

III. Start MySQL Server `mysql.server start` (for OS X termianl)

> If you use MySQL on Windows Environment, you can use `MySQL Notifier` instead of this step.

> If you use Ubuntu, mysql server will start automatically. If not, enter `sudo service mysql start` on terminal.

 
IV. Add schema and data in database

From the project root directory run : 

`mysql -usquirrel -p < olympics_demo.sql`
   
It contains the database's table creation, relations, etc; as well as the data necessary. It might take a while due to the big amount of information.

## 3. To run:

I. From the `web_server` directory run:

  `nodemon app.js`

> It will run by default on port 3000, you can change it by editing the file `app.js`. Also note that the regular `node` is not used, because the `nodemon` allows to modify a file without restarting the server, it updates the files on the fly.

II. Visualize

Go to your browser (Chrome is preferred) and type `localhost:3000`. Also, we provide an online server under the address: [`db.arifjafer.com`](http://www.db.arifjafer.com).

III. Play around

To login on the admin page of our website, use `admin@squirrel.com` and `squirrel` as a email and password. 

> Note that there is no way to create a new user because, we think that it is no necessary, since only the database mantainers should be able to modify this data. The only way to create an user is `INSERT`ing it into a table in the database called `users`.

===========

#### NOTE: Please run the following command after getting a pull:

First check if there is any files in `webserver/config/db/`

If there is execute the following:

	`git ls-files -ci --exclude-standard -z | xargs -0 git rm --cached`

Else:

	`cp development.js webserver/config/db/.`
	`cp production.js webserver/config/db/.`

	Update your db setting here (it is included in .gitignore - thus will not be uploaded to git

END

###Current Schema:
![Current Schema](/new_schema.png)

**Server, Node.js**	
* Admin operations
- `POST /admin/olympics` --> create
- `PATCH /admin/olympics/:id` --> update
- `DELETE /admin/olympics/:id` --> Delete


**Client, Angular.js**
- `localhost:3001/admin/olympics/new`
- `localhost:3001/admin/olympics/1/edit`
- `localhost:3001/admin/olympics/1/delete`


#Rest API

* `Olympics/:olympic_id/athletes/:athletes_id`
* `Olympics/:olympic_id/results/:results_id`

* `Country/:country_id/athletes/:athletes_id`
* `Country/:country_id/results/:results_id`
 

#### Sample URL queries (Try them in your browser)
`http://localhost:3001/olympics/1992_summer/results`
`http://localhost:3001/olympics/1992_summer/`
`http://localhost:3001/olympics/1992,summer/sports`
